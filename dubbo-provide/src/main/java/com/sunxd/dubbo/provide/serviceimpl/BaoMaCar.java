package com.sunxd.dubbo.provide.serviceimpl;

import com.sunxd.common.api.service.Car;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author 测试
 */
@Service
public class BaoMaCar implements Car {
    @Override
    public String getName() {
        return "宝马（BWM）";
    }
}
