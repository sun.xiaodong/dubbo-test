package com.sunxd.testpro.job;

/**
 * @author: 作者名称
 * @date: 2020-10-21 09:55
 **/

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobAnnotation {

    String lockPath();

}
