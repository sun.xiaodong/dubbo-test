package com.sunxd.testpro.job;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 作者名称
 * @date: 2020-10-20 14:46
 **/

@Configuration
public class CuratorLock {
    private static final String ZK_ADDRESS = "127.0.0.1:2181";

    @Bean
    public CuratorFramework curatorFramework(){
        CuratorFramework curatorFramework = CuratorFrameworkFactory.newClient(ZK_ADDRESS,new RetryNTimes(3, 10000));
        curatorFramework.start();
        return curatorFramework;
    }

}