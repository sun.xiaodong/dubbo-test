package com.sunxd.testpro.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author: 作者名称
 * @date: 2020-10-21 10:36
 **/
@Component
@Slf4j
public class JobTest7 {

    @Scheduled(cron = "2/5 * * * * ?")
    @JobAnnotation(lockPath = "abc")
    public void  test(){
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行定时任务 JobTest7");
    }

}
