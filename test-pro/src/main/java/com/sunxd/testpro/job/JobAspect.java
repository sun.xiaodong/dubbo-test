package com.sunxd.testpro.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * @auth 作者名称
 * @date: 2020-10-21 09:53
 **/
@Aspect
@Component
@Slf4j
public class JobAspect {

    @Autowired
    private CuratorFramework curatorFramework;

    @Value("${spring.application.name:dedf}")
    private String applicationName;

    @Pointcut("@annotation(com.sunxd.testpro.job.JobAnnotation)")
    public void methodAspect(){};

    @Around("methodAspect()")
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        String classPathName=joinPoint.getTarget().getClass().getName();
        String className=joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        JobAnnotation jobAnnotation = method.getAnnotation(JobAnnotation.class);

        System.out.println(applicationName);
        System.out.println(className);
        System.out.println(methodName);

        String lockPath = "/opt/zookeeper/lock";
        InterProcessMutex mutex = new InterProcessMutex(curatorFramework,lockPath);
        try{
            log.info("准备获取锁");
            boolean locked = mutex.acquire(0,TimeUnit.SECONDS);
            if(!locked){
                log.info("获取锁失败了");
                return null;
            }else{
                log.info("获取锁成功");
                return joinPoint.proceed();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            mutex.release();
        }
        return true;
    }


}
