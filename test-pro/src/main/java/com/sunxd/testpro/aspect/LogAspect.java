package com.sunxd.testpro.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-24
 **/
@Slf4j
@Component
@Aspect
public class LogAspect {

    @Pointcut("execution(public * com.sunxd.testpro.service..*.*(..))")
    public void pointCut(){

    }

    @Before("pointCut()")
    public void before(JoinPoint joinPoint){
        log.info("aspect before");
    }
    @After("pointCut()")
    public void after(JoinPoint joinPoint){
        log.info("aspect after");
    }
    @AfterThrowing("pointCut()")
    public void afterThrow(JoinPoint joinPoint){
        log.info("aspect afterThrow");
    }
    @Around(" pointCut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        log.info("aspect around");
        return pjp.proceed();
    }

}
