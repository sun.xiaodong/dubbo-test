package com.sunxd.testpro.controller;

import com.sunxd.testpro.service.AuthService;
import com.sunxd.testpro.service.MemeberManageService;
import org.aspectj.lang.annotation.Around;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-24
 **/
@RestController
@RequestMapping("/api/aspect/test")
public class AspectTestController {

    @Autowired
    private MemeberManageService memeberManageService;
    @Autowired
    private AuthService authService;

    @GetMapping("login")
    public String login() throws Exception {
        authService.login();
        return "ab";
    }
    @GetMapping("loginOut")
    public String loginOut() {
        authService.loginOut();
        return "ab";
    }

    @GetMapping("add")
    public String add() {
        memeberManageService.add(new AuthService.Member());
        return "ab";
    }
    @GetMapping("update")
    public String update() {
        memeberManageService.update(new AuthService.Member());
        return "ab";
    }
    @GetMapping("delete")
    public String delete() throws Exception {
        memeberManageService.delete();
        return "ab";
    }
    @GetMapping("query")
    public String query() throws Exception {
        memeberManageService.query();
        return "ab";
    }


}
