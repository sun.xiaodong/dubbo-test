package com.sunxd.testpro.jdk.proxy;

/**
 * @author: 作者名称
 * @date: 2020-07-07 15:04
 **/
public interface MyPerson {

    String  getSex();

    String getName();

    void findLove();
}
