package com.sunxd.testpro.jdk.proxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author: 作者名称
 * @date: 2020-07-09 15:49
 **/
public class MyCustomInvocationHandler implements MyInvocationHandler {

    MyPerson myPerson;

    public MyCustomInvocationHandler(MyPerson myPerson) {
        this.myPerson = myPerson;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            System.out.println("before");
            method.invoke(myPerson, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
