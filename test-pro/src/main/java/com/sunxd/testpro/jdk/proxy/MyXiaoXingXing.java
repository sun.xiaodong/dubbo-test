package com.sunxd.testpro.jdk.proxy;

/**
 * @author: 作者名称
 * @date: 2020-07-07 16:36
 **/
public class MyXiaoXingXing implements MyPerson {


    @Override
    public String getSex() {
        return "男";
    }

    @Override
    public String getName() {
        return "小星星";
    }

    @Override
    public void findLove() {
        System.out.println("xiaoxingxing  寻找真爱");
    }

}
