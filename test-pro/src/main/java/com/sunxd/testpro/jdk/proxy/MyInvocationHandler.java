package com.sunxd.testpro.jdk.proxy;

import java.lang.reflect.Method;

/**
 * @author: 作者名称
 * @date: 2020-07-08 14:36
 **/
public interface MyInvocationHandler {
    Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable;
}
