package com.sunxd.testpro.jdk.proxy;

/**
 * @author: 作者名称
 * @date: 2020-07-09 15:53
 **/
public class Test {
    public static void main(String[] args) {
        MyPerson  service = (MyPerson) MyProxy.newProxyInstance(Test.class.getClassLoader()
                , MyPerson.class, new MyCustomInvocationHandler(new MyXiaoXingXing()));
        try {
            //service是一个生成的代理类
            service.findLove();
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
