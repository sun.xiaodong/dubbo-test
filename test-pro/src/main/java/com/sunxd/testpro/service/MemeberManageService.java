package com.sunxd.testpro.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-24
 **/
@Slf4j
@Service
public class MemeberManageService {

    public boolean add(AuthService.Member member){
        log.info("log");
        return true;
    }
    public boolean update(AuthService.Member member){
        log.info("update");
        return true;
    }
    public boolean delete() throws Exception{
        log.info("delete");
        throw new Exception("自己跑出的异常");
    }
    public boolean query(){
        log.info("query");
        return true;
    }

}
