package com.sunxd.testpro.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-24
 **/
@Slf4j
@Service
public class AuthService {

    public Member login(){
        log.info("user log in");
        return null;
    }
    public boolean loginOut(){
        log.info("user log out");
        return true;
    }

    public static class  Member{
        private String name;
        private String password;

        public Member() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
