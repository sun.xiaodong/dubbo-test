package com.sunxd.testpro.reflect;

import lombok.Data;

import java.util.Date;

/**
 * @author: 作者名称
 * @date: 2020-10-12 17:49
 **/
@Data
public class Person {

    private int age;
    private String name;

    public Person() {
        System.out.println("执行默认的无参构造器");
    }

    public Person(int age) {
        System.out.println("年领=" + age);
    }

    public Person(int age, String name) {
        System.out.println("年领=" + age + " 名称=" + name);
    }

    private Person(String name) {
        System.out.println("名称=" + name);
    }



    public void m1(){
        System.out.println("m1");
    }
    public void m2(String name){
        System.out.println("m2 = "+ name);
    }
    public void m3(String name,int age){
        System.out.println("m3 = "+ name + " age = " + age);
    }

    private void m4(Date date){
        System.out.println("m4 = " + date);
    }

    public static void m5(){
        System.out.println("static m5");
    }


    public static void m6(String [] strs){
        System.out.println(strs.length);
    }
}
