package com.sunxd.testpro;

import com.alibaba.fastjson.JSON;
import com.sunxd.testpro.service.AuthService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-28
 **/
@SpringBootTest
public class AuthServiceTest extends DemoApplicationTests {

    @Autowired
    private AuthService authService;

    @Test
    void contextLoads() {
        authService.loginOut();
    }

}
