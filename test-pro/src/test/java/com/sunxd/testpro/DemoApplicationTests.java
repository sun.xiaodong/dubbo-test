package com.sunxd.testpro;

import com.alibaba.fastjson.JSON;
import com.sunxd.testpro.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;


/**
 * @description: 描述
 * @author: sun.xd
 * @create: 2020-06-24
 **/
@Slf4j
public class DemoApplicationTests {
    private static final Logger LOG = LoggerFactory.getLogger(DemoApplicationTests.class);

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


//    @BeforeAll
//    public static void beforeAll(){
//        LOG.info("beforeAll");
//    }
//
//    @BeforeEach
//    public void beforeEach(){
//        LOG.info("beforeEach");  //mockMvc = MockMvcBuilders.standaloneSetup(new IndexController()).build();
//    }

//    @AfterEach
//    public void afterEach(){
//        LOG.info("afterEach");
//    }
//
//    @AfterAll
//    public static void afterAll(){
//        LOG.info("afterAll");
//    }

}
