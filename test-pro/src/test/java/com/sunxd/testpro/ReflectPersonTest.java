package com.sunxd.testpro;

import com.sunxd.testpro.reflect.Person;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 反射测试
 *
 * @author: 作者名称
 * @date: 2020-10-12 17:54
 **/

public class ReflectPersonTest {



    // 方法
    //static方法
    @Test
    public void method6() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Method m = clazz.getMethod("m6", null);
        m.invoke(null,null);
    }

    //static方法
    @Test
    public void method5() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Method m = clazz.getMethod("m5", null);
        m.invoke(null,null);
    }
    //私有方法
    @Test
    public void method4() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Method m = clazz.getDeclaredMethod("m4", Date.class);
        m.setAccessible(true);
        m.invoke(clazz.newInstance(),new Date());
    }
    //2个参数
    @Test
    public void method3() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Method m = clazz.getMethod("m3", String.class,int.class);
        m.invoke(clazz.newInstance(),"name",12);
    }
    //一个参数
    @Test
    public void method2() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Method m = clazz.getMethod("m2", String.class);
        m.invoke(clazz.newInstance(),"name");
    }
    //无参
    @Test
    public void method1() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Person person = (Person) clazz.newInstance();
        Method m = clazz.getMethod("m1");
        m.invoke(clazz.newInstance(), (Object) null);
        //huo
        m.invoke(person, (Object) null);
    }



    //--------- 构造方法


    //获得所有的构造函数
    @Test
    public void constractor5() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Constructor [] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors){
            System.out.println(constructor);
        }
    }
    /**
     * 无参构造器反射
     *
     * @throws Exception
     */
    @Test
    public void constractor1() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Constructor constructor = clazz.getConstructor(null);
        Person p = (Person) constructor.newInstance(null);
        System.out.println(p.getClass());
        System.out.println(Person.class);
        System.out.println(Class.forName("com.sunxd.testpro.reflect.Person"));
    }

    /**
     * 单参构造器反射
     * @throws Exception
     */
    @Test
    public void constractor2() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Constructor constructor = clazz.getConstructor(int.class);
        constructor.newInstance(3);
    }

    /**
     * 双参构造器反射
     * @throws Exception
     */
    @Test
    public void constractor3() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Constructor constructor = clazz.getConstructor(int.class,String.class);
        constructor.newInstance(3,"34");
    }

    /**
     * 私有 单参构造器反射
     * @throws Exception
     */
    @Test
    public void constractor4() throws Exception {
        Class<?> clazz = Class.forName("com.sunxd.testpro.reflect.Person");
        Constructor constructor = clazz.getDeclaredConstructor(String.class);
        constructor.setAccessible(true);
        constructor.newInstance("ad");
    }
}
