package com.sunxd.common.api.service;

public interface Car {

    /**
     * 获取名称
     * @return
     */
    String getName();

}
