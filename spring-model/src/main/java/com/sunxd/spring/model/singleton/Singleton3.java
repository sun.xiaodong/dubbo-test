package com.sunxd.spring.model.singleton;

/**
 * 饿汉模式（线程安全）
 * 直接在运行这个类的时候进行一次loading，之后直接访问。显然，这种方法没有起到lazy loading的效果，考虑到前面提到的和静态类的对比，这种方法只比静态类多了一个内存常驻而已
 * @author: 作者名称
 * @date: 2020-07-13 16:45
 **/
public class Singleton3 {

    private static Singleton3 singleton3 = new Singleton3();

    private Singleton3() {
    }

    public static Singleton3 getInstance() {
        return singleton3;
    }
}
