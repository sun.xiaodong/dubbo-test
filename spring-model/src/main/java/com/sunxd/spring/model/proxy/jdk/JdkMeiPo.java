package com.sunxd.spring.model.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author: 作者名称
 * @date: 2020-07-07 16:47
 **/
public class JdkMeiPo implements InvocationHandler {

    private Person target;

    public Object getInstance(Person target){
        this.target = target;
        Class<? extends Person> clazz = target.getClass();
        System.out.println("被代理的对象 clazz" + clazz);
        return Proxy.newProxyInstance(clazz.getClassLoader(),clazz.getInterfaces(),this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("媒婆开始筛选： 符合条件的");
        method.invoke(this.target,args);
        System.out.println("媒婆开始筛选 结束");
        return null;
    }


}
