package com.sunxd.spring.model.proxy.cglib;

/**
 * @author: 作者名称
 * @date: 2020-07-10 10:53
 **/
public class CglibTargetObject {

    public void findLove(){
        System.out.println( " CglibTargetObject find love" );
    }
}
