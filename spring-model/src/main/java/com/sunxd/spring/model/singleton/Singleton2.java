package com.sunxd.spring.model.singleton;

/**
 *
 * 线程安全的懒汉模式（线程安全
 * 而并发其实是一种特殊情况，大多时候这个锁占用的额外资源都浪费了，这种打补丁方式写出来的结构效率很低。
 * @author: 作者名称
 * @date: 2020-07-13 16:44
 **/
public class Singleton2 {
    private static  Singleton2 singleton1;

    private Singleton2() {

        System.out.println(1);
        System.out.println(2);
        System.out.println(3);
    }

    public static synchronized Singleton2 getInstance(){
        if (singleton1 == null){
            singleton1 = new Singleton2();
        }
        return singleton1;
    }


}
