package com.sunxd.spring.model.proxy.cglib;

/**
 * @author: 作者名称
 * @date: 2020-07-10 11:08
 **/
public class CglibTest {

    public static void main(String[] args) {
        System.out.println(CglibTargetObject.class.getName());
        CglibTargetObject o = (CglibTargetObject) new CglibMeipo().getInstance(CglibTargetObject.class);
        o.findLove();
    }

}
