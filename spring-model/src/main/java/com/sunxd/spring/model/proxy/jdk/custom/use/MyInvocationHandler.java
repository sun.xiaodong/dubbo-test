package com.sunxd.spring.model.proxy.jdk.custom.use;

import java.lang.reflect.Method;

/**
 * @author: 作者名称
 * @date: 2020-07-09 17:02
 **/
public interface MyInvocationHandler {

    /**
     * invoke
     *
     * @param proxy  指被代理的对象
     * @param method 要调用的方法
     * @param args   方法调用时所需要的参数
     * @return Object
     * @throws Throwable 异常
     */
    Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
