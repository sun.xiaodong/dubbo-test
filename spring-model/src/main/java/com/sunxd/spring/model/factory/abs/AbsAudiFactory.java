package com.sunxd.spring.model.factory.abs;

import com.sunxd.spring.model.factory.Audi;
import com.sunxd.spring.model.factory.Car;
import com.sunxd.spring.model.factory.fun.Factory;

/**
 * @author: 作者名称
 * @date: 2020-07-10 17:39
 **/
public class AbsAudiFactory extends AbstractFactory {
    @Override
    public Car getCar() {
        return new Audi();
    }
}
