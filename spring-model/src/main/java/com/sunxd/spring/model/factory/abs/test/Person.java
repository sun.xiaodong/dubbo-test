package com.sunxd.spring.model.factory.abs.test;

/**
 * @author: 作者名称
 * @date: 2020-07-13 11:37
 **/

public abstract class Person {

    public abstract void getSex();

    public abstract void getName();


    public String getInfo(){
        getName();
        getSex();
        return " information ";
    }

}
