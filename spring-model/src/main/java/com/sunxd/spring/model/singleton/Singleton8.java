package com.sunxd.spring.model.singleton;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 使用CAS锁实现（线程安全）
 *
 * @author:
 * @date: 2020-07-13 18:26
 **/
public class Singleton8 {

    private static final AtomicReference<Singleton8> INSTANCE = new AtomicReference<>();

    private Singleton8() {
    }


    public static final Singleton8 getInstance() {
        for (; ; ) {
            Singleton8 singleton8 = INSTANCE.get();
            if (singleton8 == null) {
                singleton8 = new Singleton8();
                INSTANCE.compareAndSet(null, singleton8);
                return singleton8;
            }
            return singleton8;
        }
    }

}
