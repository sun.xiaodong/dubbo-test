package com.sunxd.spring.model.singleton;

/**
 * @author: 双重校验锁法（通常线程安全，低概率不安全）
 * @date: 2020-07-13 17:25
 **/
public class Singleton6 {

    private static Singleton6 singleton6;

    public static Singleton6 getInstance(){
        if(singleton6 == null){
            synchronized (Singleton6.class){
                if(singleton6 == null){
                    singleton6 = new Singleton6();
                }
            }
        }
        return singleton6;
    }

}
