package com.sunxd.spring.model.factory.abs.test;

/**
 * @author: 作者名称
 * @date: 2020-07-13 11:39
 **/
public class Woman extends Person {
    @Override
    public void getSex() {
        System.out.println(  "女");
    }

    @Override
    public void getName() {
        System.out.println( "女 name");
    }
}
