package com.sunxd.spring.model.delegate;

import org.springframework.stereotype.Component;

/**
 * @author: 作者名称
 * @date: 2020-07-14 11:31
 **/
@Component
public class Woman implements IPerson {
    @Override
    public String getSex() {
        return "woman";
    }
}
