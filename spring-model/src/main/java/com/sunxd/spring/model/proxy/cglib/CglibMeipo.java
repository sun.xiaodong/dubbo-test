package com.sunxd.spring.model.proxy.cglib;


import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author: 作者名称
 * @date: 2020-07-10 09:50
 **/
public class CglibMeipo implements MethodInterceptor {


    /**
     *
     * @param clazz
     * @return
     */
    public Object getInstance(Class clazz){

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        // 这个obj的引用是由CGLIB给我们new出来的，
        //cglib new出来以后的对象，是被代理对象的子类，（继承了我们自己写的那个类）
        //OOP ， new子类之前，实际上默认先调用我们super（）方法的，
        // new了子类的同时，必须先new父类，这就想当于是间接的持有了我们父类的引用，子类默认重写父类所有的方法
        // 我们改变子类对象的某些属性，是可以间接操作父类的属性
        System.out.println("cglib intercept");
        methodProxy.invokeSuper(o,objects);
        return null;
    }
}
