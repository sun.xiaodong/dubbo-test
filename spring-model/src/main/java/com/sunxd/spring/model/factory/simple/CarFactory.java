package com.sunxd.spring.model.factory.simple;

import com.sunxd.spring.model.factory.Audi;
import com.sunxd.spring.model.factory.Bmw;
import com.sunxd.spring.model.factory.Car;

/**
 * @author: 作者名称
 * @date: 2020-07-10 15:46
 **/
public class CarFactory {

    // 实现统一管理，专业化管理
    public Car getCar(String name){
        if("bmw".equalsIgnoreCase(name)) {
            return new Bmw();
        }else if("audi".equalsIgnoreCase(name)){
            return new Audi();
        }
        return null;
    }
}
