package com.sunxd.spring.model.proxy.jdk;

/**
 * @author: 作者名称
 * @date: 2020-07-07 16:36
 **/
public class XiaoXingXing implements Person {


    public String getSex() {
        return "男";
    }

    public String getName() {
        return "小星星";
    }

    @Override
    public void findLove() {
        System.out.println("xiaoxingxing  寻找真爱");
    }

}
