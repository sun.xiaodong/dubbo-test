package com.sunxd.spring.model.strategy;

import org.springframework.cglib.core.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: 作者名称
 * @date: 2020-07-14 15:11
 **/
public class StrategyTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>(){{add(1);add(3);add(2);}};
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return 0;
            }
        });
    }
}
