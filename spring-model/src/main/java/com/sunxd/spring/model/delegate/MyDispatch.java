package com.sunxd.spring.model.delegate;

/**
 * @author: 作者名称
 * @date: 2020-07-14 11:57
 **/
public class MyDispatch {

    private IPerson iPerson;

    public MyDispatch(IPerson iPerson) {
        this.iPerson = iPerson;
    }

    public void doing(){
        this.iPerson.getSex();
    }
}
