package com.sunxd.spring.model.factory.fun;

import com.sunxd.spring.model.factory.Car;

/**
 * 工厂接口  定义所有工厂的执行标准
 * @author: 作者名称
 * @date: 2020-07-10 17:38
 **/
public interface Factory {
    /**
     * 标准
     *  尾气标准
     *  安全标准
     *  车架标准
     * @return
     */
    Car getCar();

}
