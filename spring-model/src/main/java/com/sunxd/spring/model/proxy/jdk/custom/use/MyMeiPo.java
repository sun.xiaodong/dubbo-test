package com.sunxd.spring.model.proxy.jdk.custom.use;

import com.sunxd.spring.model.proxy.jdk.Person;

import java.lang.reflect.Method;

/**
 * @author: 作者名称
 * @date: 2020-07-09 17:05
 **/
public class MyMeiPo implements MyInvocationHandler {
    private Person target;

    /**获取被代理人的个人资料
     * @return Object
     */
    public Object getInstance(Person target) {
        this.target = target;
        Class<? extends Person> clazz = target.getClass();
        return MyProxy.newProxyInstance(new MyClassLoader(), clazz.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("我是媒婆，按照你的要求");
        System.out.println("开始进行海选...");
        System.out.println("------------");
        //调用的时候
        method.invoke(proxy, args);
        System.out.println("------------");
        System.out.println("选择结束，如果合适的话，就准备办事");
        return null;
    }
}
