package com.sunxd.spring.model.factory.abs.test;

/**
 * @author: 作者名称
 * @date: 2020-07-13 11:39
 **/
public class Man extends Person {
    @Override
    public void getSex() {
        System.out.println( "男");
    }

    @Override
    public void getName() {
        System.out.println( "nan name");
    }
}
