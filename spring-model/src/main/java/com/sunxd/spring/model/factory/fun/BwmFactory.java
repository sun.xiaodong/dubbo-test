package com.sunxd.spring.model.factory.fun;

import com.sunxd.spring.model.factory.Bmw;
import com.sunxd.spring.model.factory.Car;

/**
 * @author: 作者名称
 * @date: 2020-07-10 17:39
 **/
public class BwmFactory implements Factory {
    @Override
    public Car getCar() {
        return new Bmw();
    }
}
