package com.sunxd.spring.model.factory.abs;

import com.sunxd.spring.model.factory.Car;

/**
 * @author: 作者名称
 * @date: 2020-07-10 17:59
 **/
public class AbsTest {

    public static void main(String[] args) {
        AbstractFactory defaultAbstractFactory = new DefaultAbstractFactory();
        System.out.println(defaultAbstractFactory.getCar().getName());
    }
}
