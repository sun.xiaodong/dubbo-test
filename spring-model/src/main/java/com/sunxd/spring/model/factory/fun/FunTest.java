package com.sunxd.spring.model.factory.fun;

/**
 * @author: 作者名称
 * @date: 2020-07-10 17:42
 **/
public class FunTest {

    public static void main(String[] args) {
        // 工厂方法模式
        //产品各自都拥有自己的工厂
        Factory factory = new AudiFactory();
        System.out.println(factory.getCar().getName());

        factory = new BwmFactory();
        System.out.println(factory.getCar().getName());
        // 增加代码的复杂度



    }
}
