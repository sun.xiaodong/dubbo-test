package com.sunxd.spring.model.factory.abs;

import com.sunxd.spring.model.factory.Car;

/**
 * @author: 作者名称
 * @date: 2020-07-10 18:00
 **/
public class DefaultAbstractFactory extends AbstractFactory {


    @Override
    protected Car getCar() {
        return new AbsAudiFactory().getCar();
    }
}
