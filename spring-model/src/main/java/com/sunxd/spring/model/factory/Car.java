package com.sunxd.spring.model.factory;

/**
 * @author: 作者名称
 * @date: 2020-07-10 15:06
 **/
public interface Car {

    String getName();
}
