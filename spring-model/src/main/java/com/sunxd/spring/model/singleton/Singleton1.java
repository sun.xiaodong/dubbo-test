package com.sunxd.spring.model.singleton;

/**
 * @author: 懒汉模式（线程不安全
 * 缺点是，没有考虑到线程安全，可能存在多个访问者同时访问，并同时构造了多个对象的问题。之所以叫做懒汉模式，主要是因为此种方法可以非常明显的lazy loading
 * @date: 2020-07-13 16:41
 **/
public class Singleton1 {

    private static  Singleton1 singleton1;

    private Singleton1() {
    }

    public static Singleton1 getInstance(){
        if (singleton1 == null){
            singleton1 = new Singleton1();
        }
        return singleton1;
    }
}
