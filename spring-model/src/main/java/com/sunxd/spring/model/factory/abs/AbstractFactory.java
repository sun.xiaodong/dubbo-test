package com.sunxd.spring.model.factory.abs;

import com.sunxd.spring.model.factory.Car;

/**
 * @author: 作者名称
 * @date: 2020-07-10 17:54
 **/
public abstract class AbstractFactory {

    protected abstract Car  getCar();

    public Car getCar(String name){
        if("bmw".equalsIgnoreCase(name)) {
            return new AbsBwmFactory().getCar();
        }else if("audi".equalsIgnoreCase(name)){
            return new AbsAudiFactory().getCar();
        }
        return null;
    }
}
