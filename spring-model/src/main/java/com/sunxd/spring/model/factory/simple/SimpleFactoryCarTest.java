package com.sunxd.spring.model.factory.simple;

import com.sunxd.spring.model.factory.Car;
import com.sunxd.spring.model.factory.simple.CarFactory;

/**
 * @author: 作者名称
 *
 * @date: 2020-07-10 15:48
 **/
public class SimpleFactoryCarTest {


    public static void main(String[] args) {
        Car car = new CarFactory().getCar("Audi");
        System.out.println(car.getName());
    }
}
