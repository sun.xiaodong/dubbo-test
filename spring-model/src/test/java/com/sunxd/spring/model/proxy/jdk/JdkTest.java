package com.sunxd.spring.model.proxy.jdk;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author: 作者名称
 * @date: 2020-07-07 16:50
 **/

public class JdkTest {


    @Test
    @Ignore
    public void test(){
        new XiaoXingXing().findLove();
    }

    @Test
    public void jdkTest() throws IOException {
        Person person = (Person) new JdkMeiPo().getInstance(new XiaoXingXing());
        System.out.println("代理后的对象" + person.getClass());
//        person.findLove();
//        person.getSex();

//        byte [] data = ProxyGenerator.generateProxyClass("$Proxy0000", new Class[]{Person.class});
//        FileOutputStream os = new FileOutputStream("$Proxy111.class");
//        os.write(data);
//        os.close();


        // 1 拿到被代理对象的引用，然后获取他的接口@
        // 2 JDK代理重新生成一个类，同时实现我们给的代理对象所实现的接口
        // 3 把被代理对象的饮用也拿到了
        // 4 重新动态生成一个class字节码
        // 5 然后编译
    }
}
