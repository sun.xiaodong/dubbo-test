package com.sunxd.springredistest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.http.ResponseEntity;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class SpringRedisTestApplicationTests {


    @Resource
    private DefaultRedisScript<Boolean> redisScript;

    @Resource
    private RedisTemplate<String,String> redisTemplate;


    @Test
    void contextLoads() {
        List<String> keys = Arrays.asList("testLua1", "hello lua");
        Boolean execute = redisTemplate.execute(redisScript, keys, 100);
        System.out.println(execute);
    }

    @Test
    void testRemove(){
        redisTemplate.delete("testLua1");
    }

}
